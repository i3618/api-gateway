package services

import (
	"fmt"
	"tasks/Telegram_Message_Sender/api-gateway/config"
	sp "tasks/Telegram_Message_Sender/api-gateway/genproto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	SenderService() sp.SenderServiceClient
}

// Client
type GrpcClient struct {
	cfg         config.Service
	connections map[string]interface{}
}

func NewServiceManager(cfg *config.Service) (*GrpcClient, error) {

	connSender, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.SenderServiceHost, cfg.SenderServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &GrpcClient{
		cfg: *cfg,
		connections: map[string]interface{}{
			"sender_service": sp.NewSenderServiceClient(connSender),
		},
	}, nil
}

func (g *GrpcClient) SenderService() sp.SenderServiceClient {
	return g.connections["sender_service"].(sp.SenderServiceClient)
}
