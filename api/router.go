package api

import (
	_ "tasks/Telegram_Message_Sender/api-gateway/api/docs"
	hs "tasks/Telegram_Message_Sender/api-gateway/api/handlers"
	"tasks/Telegram_Message_Sender/api-gateway/config"
	"tasks/Telegram_Message_Sender/api-gateway/pkg/logger"
	"tasks/Telegram_Message_Sender/api-gateway/services"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
)

type Options struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
}

// @BasePath /bot
// @version 1.0
// @description This api for sending message to bot

func NewRouter(option Options) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handler := hs.New(&hs.HandlerConfig{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
	})

	url := router.Group("/bot")
	url.POST("/send", handler.SendMessage)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	return router
}
