package handlers

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"tasks/Telegram_Message_Sender/api-gateway/api/models"
	sp "tasks/Telegram_Message_Sender/api-gateway/genproto"
	"time"

	"github.com/gin-gonic/gin"
)

// MessageToSender godoc
// @Summary Message
// @Description This method for send message to bot
// @Tags Bot
// @Accept json
// @Produce json
// @Param todo body models.MessageReq true "Message"
// @Success 200 {object} models.Res
// @Failure 400 {object} sender_proto.Err
// @Failure 500 {object} sender_proto.Err
// @Router /send [POST]
func (h *handler) SendMessage(c *gin.Context) {
	var (
		now      string
		Priority int64
	)
	body := models.MessageReq{}
	err := c.ShouldBindJSON(&body)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, sp.Err{Message: "Error while parse body"})
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(5))
	defer cancel()

	switch strings.ToLower(body.Prioritie) {
	case "low":
		Priority = 3
	case "medium":
		Priority = 2
	case "hight":
		Priority = 1
	default:
		c.JSON(http.StatusBadRequest, sp.Err{Message: "Something wrong!"})
		return
	}

	now = time.Now().Format(time.RFC3339)

	_, err = h.serviceManager.SenderService().SendMessage(ctx, &sp.MessageReq{Text: body.Text, Priority: Priority, CreatedAt: now})
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusInternalServerError, sp.Err{Message: "Internal error"})
		return
	}

	c.JSON(http.StatusOK, models.Res{Message: "Ok!"})
}
