package handlers

import (
	"tasks/Telegram_Message_Sender/api-gateway/config"
	"tasks/Telegram_Message_Sender/api-gateway/pkg/logger"
	"tasks/Telegram_Message_Sender/api-gateway/services"
)

type handler struct {
	serviceManager services.IServiceManager
	log            logger.Logger
	cfg            config.Config
}

// HandlerV1Config ...
type HandlerConfig struct {
	ServiceManager services.IServiceManager
	Logger         logger.Logger
	Cfg            config.Config
}

// New ...
func New(c *HandlerConfig) *handler {
	return &handler{
		serviceManager: c.ServiceManager,
		log:            c.Logger,
		cfg:            c.Cfg,
	}
}
