CURRENT_DIR=$(shell pwd)

proto-gen:
	./scripts/proto-gen.sh ${CURRENT_DIR}
	ls genproto/ | xargs -n1 -IX bash -c "sed -e '/bool/ s/,omitempty//' X > X.tmp && mv X{.tmp,}"

swag-gen:
	swag init -g api/router.go -o api/docs
