package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Services Service
	Lpe      ConfigP
}
type Service struct {
	SenderServiceHost string
	SenderServicePort int
}
type ConfigP struct {
	Enivorentment string
	LogLevel      string
	Port          string
}

func LoadConfig() Config {
	c := Config{}

	c.Lpe.Enivorentment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.Lpe.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.Lpe.Port = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	c.Services.SenderServiceHost = cast.ToString(getOrReturnDefault("SENDER_SERVICE_HOST", "localhost"))
	c.Services.SenderServicePort = cast.ToInt(getOrReturnDefault("SENDER_SERVICE_PORT", 9000))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
